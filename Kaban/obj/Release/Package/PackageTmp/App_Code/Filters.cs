﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Kaban.App_Code
{
    public class Filters
    {
        public class OnlySignedAttribute : FilterAttribute, IActionFilter
        {
            //Before controller method called
            void IActionFilter.OnActionExecuting(ActionExecutingContext FilterContext)
            {
                var Session = FilterContext.HttpContext.Session;
                if (Session["Id"] == null)
                {
                    var CurrentUrl = new UrlHelper(FilterContext.RequestContext);
                    var LoginUrl = CurrentUrl.Content("~/Main/Login");
                    FilterContext.HttpContext.Response.Redirect(LoginUrl, true);
                }
            }

            //After controller method called
            void IActionFilter.OnActionExecuted(ActionExecutedContext filterContext)
            {
                //filterContext.Controller.ViewBag.OnActionExecuted = "IActionFilter.OnActionExecuted filter called";
            }
        }
    }
}
﻿using Outdoor.App_Code;
using Outdoor.Models;
using Outdoor.Models.Auth;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using static Outdoor.App_Code.Filters;

namespace Outdoor.Controllers
{
    [Culture]
    public class AuthController : Controller
    {
        //Login
        public ActionResult Index()
        {
            if (Session["Id"] != null)
            {
                return RedirectToAction("Index", "Home");
            }
            
            if(Session["Reg"] != null)
            {
                ViewBag.R = Resources.Resource.RegistrationCompleted;
                Session.Abandon();
            }
            ViewBag.Test = Resources.Resource.ErrorEmptyLogin;
            ViewBag.Test2 = Resources.Resource.ButtonAccept;
            return View();
        }

        [HttpPost]
        public ActionResult Index(AuthFormModel Model)
        {
            if (Session["Id"] != null)
            {
                return RedirectToAction("Index", "Home");
            }
            if (ModelState.IsValid)
            {
                DatabaseModelDataContext db = new DatabaseModelDataContext();
                Users User = (from Users in db.Users
                              where Users.Login == Model.Login select Users).FirstOrDefault();

                if (User != null)
                {
                    if (User.Password == Security.SHA512(Model.Password))
                    {
                        Session["Id"] = Session.SessionID;
                        return RedirectToAction("Index", "Home");
                    }
                    ModelState.AddModelError(string.Empty, Resources.Resource.PasswordorLoginIncorrect);
                }
                else
                {
                    ModelState.AddModelError(string.Empty, Resources.Resource.PasswordorLoginIncorrect);
                }
            }
            return View(Model);
        }
        //Registration
        public ActionResult Registration()
        {
            if (Session["Id"] != null)
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }
        [HttpPost]
        public ActionResult Registration(RegistrationFormModel Model)
        {
            if (ModelState.IsValid)
            {
                DatabaseModelDataContext db = new DatabaseModelDataContext();
                Users User = (from Users in db.Users where Users.Login == Model.Login select Users).FirstOrDefault();

                if (User != null)
                {
                    ModelState.AddModelError(string.Empty, Resources.Resource.UserExist); 
                }
                else
                {
                    if(Model.Password.Length < 5)
                    {
                        ModelState.AddModelError(string.Empty, Resources.Resource.PasswordLength);               
                    }
                    else
                    {
                        if(Model.Password == Model.Password2)
                        {
                            var AddUser = new Users { Id = Guid.NewGuid(), Login = Model.Login, Password = Security.SHA512(Model.Password),Mail = Model.Email};
                            db.Users.InsertOnSubmit(AddUser);
                            db.SubmitChanges();
                            Session["Reg"] = "Done";
                            return RedirectToAction("Index", "Auth");
                        }
                        ModelState.AddModelError(string.Empty, Resources.Resource.PasswordReenter);
                    }
                }
            }
            return View(Model);
        }
        //Reset Password 
        public ActionResult ResetPassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ResetPassword(ResetPasswordFormModel Model)
        {
            if (ModelState.IsValid)
            {
                DatabaseModelDataContext db = new DatabaseModelDataContext();
                Users User = (from Users in db.Users where Users.Login == Model.Login select Users).FirstOrDefault();

                if (User != null)
                {
                    Random rnd = new Random();
                    int secretPass = rnd.Next(10000000, 99999999);
                    string spass = secretPass.ToString();

                    MailAddress from = new MailAddress("kononov.l.y@gmail.com", "Leonid");
                    MailAddress to = new MailAddress(User.Mail);
                    // создаем объект сообщения
                    MailMessage message = new MailMessage(from, to);
                    // тема письма
                    message.Subject = "Reset Password Request";
                    // текст письма
                    message.Body = "Your secret password :" + spass;
                    // письмо представляет код html
                    message.IsBodyHtml = true;
                    // адрес smtp-сервера и порт, с которого будем отправлять письмо
                    SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);
                    // логин и пароль
                    smtp.Credentials = new NetworkCredential("kononov.l.y@gmail.com", "**********");
                    smtp.EnableSsl = true;
                    smtp.Send(message);


                    Session["ResetPassword"] = User.Login;
                    Session["ResetPassword2"] = Security.SHA512(spass);
                    return RedirectToAction("ResetPassword2", "Auth");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, Resources.Resource.LoginIncorrect);
                }
            }
                return View(Model);
        }
        public ActionResult ResetPassword2()
        {
            if (Session["ResetPassword"] != null)
            {
            }
            else
            {
                return RedirectToAction("Index", "Auth");
            }
                return View();
        }
        [HttpPost]
        public ActionResult ResetPassword2(ResetPassword2FormModel Model)
        {
            if(Session["ResetPassword"] != null)
            {
                string PassHash = Session["ResetPassword2"].ToString();
                string Log = Session["ResetPassword"].ToString();

                if (ModelState.IsValid)
                {
                    DatabaseModelDataContext db = new DatabaseModelDataContext();
                    Users User = (from Users in db.Users where Users.Login == Log select Users).FirstOrDefault();
                    if(Security.SHA512(Model.SecretPassword) == PassHash)
                    {
                        if (Model.Password.Length < 5)
                        {
                            ModelState.AddModelError(string.Empty, Resources.Resource.PasswordLength);

                        }
                        else
                        {
                            if (Model.Password == Model.Password2)
                            {
                                User.Password = Security.SHA512(Model.Password);
                                //db.Users.InsertOnSubmit(User);
                                db.SubmitChanges();
                                Session.Abandon();
                                return RedirectToAction("Index", "Auth");
                            }
                            ModelState.AddModelError(string.Empty, Resources.Resource.PasswordReenter);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "Secret Pass incorrect");
                    }
                }
                return View(Model);
            }
            return RedirectToAction("Index", "Auth");
        }
        //Culture
        [HttpPost]
        public ActionResult ChangeCulture(string lang)
        {
            string returnUrl = Request.UrlReferrer.AbsolutePath;
            // Список культур
            List<string> cultures = new List<string>() { "pl", "en", "ru" };
            if (!cultures.Contains(lang))
            {
                lang = "en";
            }
            // Сохраняем выбранную культуру в куки
            HttpCookie cookie = Request.Cookies["lang"];
            if (cookie != null)
            {
                cookie.Value = lang;
            }  // если куки уже установлено, то обновляем значение
            else
            {
                cookie = new HttpCookie("lang");
                cookie.HttpOnly = false;
                cookie.Value = lang;
                cookie.Expires = DateTime.Now.AddYears(1);
            }
            Response.Cookies.Add(cookie);
            return Redirect(returnUrl);
        }
        public ActionResult Logout()
        {
            //kill session
            Session.Abandon();
            return RedirectToAction("Index", "Auth");
        }
    }
}
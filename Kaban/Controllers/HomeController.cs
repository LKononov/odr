﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Outdoor.Models;
using Outdoor.App_Code;
using static Outdoor.App_Code.Filters;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace Outdoor.Controllers
{   
    [Culture]
    [OnlySigned]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        //dinamical search
        [HttpGet]
        public ActionResult Ajax(string SearchText)
        {
            string SearchParam = SearchText.ToString();
            DatabaseModelDataContext db = new DatabaseModelDataContext();
            List<Users> Search = .Where().ToList();

            JObject Result = new JObject();
            Result.Add(new JProperty("Result", JToken.FromObject(Search)));
            return Content(Result.ToString(), "application/json");
        }
        //Personal Workspace
        public ActionResult Account()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Upload(IEnumerable<HttpPostedFileBase> uploads)
        {
            foreach (var file in uploads)
            {
                if (file != null)
                {
                    // получаем имя файла
                    string fileName = System.IO.Path.GetFileName(file.FileName);
                    // сохраняем файл в папку Files в проекте
                    
                    file.SaveAs(Server.MapPath("~/Content/Images/" + fileName));
                    ViewBag.Img = "~/Content/Images/" + fileName + ".jpg";
                }
            }
            return RedirectToAction("Account");
        }
    }
}
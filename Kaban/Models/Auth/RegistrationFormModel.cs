﻿
using System.ComponentModel.DataAnnotations;


namespace Outdoor.Models.Auth
{
    
    public class RegistrationFormModel
    {
        [Required(ErrorMessageResourceType = typeof(Resources.Resource),
               ErrorMessageResourceName = "ErrorEmptyLogin")]
        public virtual string Login { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Resource),
               ErrorMessageResourceName = "ErrorEmptyMail")]
        [EmailAddress(ErrorMessageResourceType = typeof(Resources.Resource),
               ErrorMessageResourceName = "MailInvalid")]
        public virtual string Email { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Resource),
               ErrorMessageResourceName = "ErrorEmptyPassword")]
        public virtual string Password { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Resource),
               ErrorMessageResourceName = "ErrorEmptyPassword")]
        public virtual string Password2 { get; set; }
    }
}
﻿
using System.ComponentModel.DataAnnotations;

namespace Outdoor.Models.Auth
{
    public class ResetPassword2FormModel
    {
        [Required(ErrorMessageResourceType = typeof(Resources.Resource),
       ErrorMessageResourceName = "ErrorEmptyPassword")]
        public virtual string Password { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Resource),
               ErrorMessageResourceName = "ErrorEmptyPassword")]
        public virtual string Password2 { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Resource),
               ErrorMessageResourceName = "ErrorEmptyPassword")]
        public virtual string SecretPassword { get; set; }
    }
}
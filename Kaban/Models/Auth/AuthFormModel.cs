﻿
using System.ComponentModel.DataAnnotations;


namespace Outdoor.Models.Auth
{

    public class AuthFormModel
    {
        [Required(ErrorMessageResourceType = typeof(Resources.Resource),
                  ErrorMessageResourceName = "ErrorEmptyLogin")]
        public virtual string Login { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Resource),
                  ErrorMessageResourceName = "ErrorEmptyPassword")]
        public virtual string Password { get; set; }
    }
}
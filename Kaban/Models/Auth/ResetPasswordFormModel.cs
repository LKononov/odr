﻿
using System.ComponentModel.DataAnnotations;


namespace Outdoor.Models.Auth
{
    public class ResetPasswordFormModel
    {
        [Required(ErrorMessageResourceType = typeof(Resources.Resource),
               ErrorMessageResourceName = "ErrorEmptyLogin")]
        public virtual string Login { get; set; }
    }
}
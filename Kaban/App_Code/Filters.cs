﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace Outdoor.App_Code
{
    public class Filters
    {
        public class OnlySignedAttribute : FilterAttribute, IActionFilter
        {
            //Before controller method called
            void IActionFilter.OnActionExecuting(ActionExecutingContext FilterContext)
            {
                var Session = FilterContext.HttpContext.Session;
                if (Session["Id"] == null)
                {
                    var CurrentUrl = new UrlHelper(FilterContext.RequestContext);
                    var LoginUrl = CurrentUrl.Content("~/Auth/Index");
                    FilterContext.HttpContext.Response.Redirect(LoginUrl, true);
                }
            }
            //After controller method called
            void IActionFilter.OnActionExecuted(ActionExecutedContext filterContext)
            {
                //filterContext.Controller.ViewBag.OnActionExecuted = "IActionFilter.OnActionExecuted filter called";
            }
        }
            public class CultureAttribute : FilterAttribute, IActionFilter
            {
                public void OnActionExecuted(ActionExecutedContext filterContext)
                {

                }

                public void OnActionExecuting(ActionExecutingContext FilterContext)
                {
                string cultureName = null;

                // Получаем куки из контекста, которые могут содержать установленную культуру
                HttpCookie cultureCookie = FilterContext.HttpContext.Request.Cookies["lang"];
                if (cultureCookie != null)
                {
                    cultureName = cultureCookie.Value;
                }
                else
                {
                    cultureName = "en";
                }
                // Список культур
                List<string> cultures = new List<string>() { "pl", "en", "ru" };
                if (!cultures.Contains(cultureName))
                {
                    cultureName = "en";
                }
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(cultureName);
                Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(cultureName);
            }
        }
    }
}

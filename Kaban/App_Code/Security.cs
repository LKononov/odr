﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Outdoor.App_Code
{
    class Security
    {
        public static string SHA512(string Message)
        {
            using (var Hash = System.Security.Cryptography.SHA512.Create())
            {
                var HashedBytes = Hash.ComputeHash(Encoding.UTF8.GetBytes(Message));

                // Convert to text
                // StringBuilder Capacity is 128, because 512 bits / 8 bits in byte * 2 symbols for byte 
                var StringBuilder = new StringBuilder(128);
                foreach (var b in HashedBytes)
                    StringBuilder.Append(b.ToString("X2"));
                return StringBuilder.ToString();
            }
        }
    }
}
